var express = require('express');
var socket = require('socket.io');

//Variables
var port = "8000";

//App
var app = express();
var server = app.listen(port, function(){
    console.log('Listening to request on port ' + port + '.');
});

//Socket
var io = socket(server);

io.on('connection', function(socket){
    
    console.log('made socket connection', socket.id);

    //Raw Material Inventory Sockets
    socket.on('rminvinformation', function(){
        socket.broadcast.emit('reloadrminvinformation');
    });

    socket.on('loadmrcount', function(){
        socket.broadcast.emit('reloadmrcount');
    });

    socket.on('rminvplanningmri', function(){
        socket.broadcast.emit('reloadplanningmrirequest');
    });

    //Planning Sockets
    socket.on('planningmrinfo', function(){
        socket.broadcast.emit('reloadmrplanninginfo');
    });

    socket.on('planningpr', function(){
        socket.broadcast.emit('reloadprpurchasing');
        socket.broadcast.emit('reloadprcount');
    });

    socket.on('cuspurchaseorder', function(){
        socket.broadcast.emit('cuspurchaseorder');
    });


    //Purchasing Sockets
    socket.on('purchasingpr', function(){
        socket.broadcast.emit('reloadprplanning');
    });

    socket.on('planningnotification', function(data){
        socket.emit('notifplanning', { data: data.prid });        
    });

    //Production Sockets
    socket.on('productiontag', function(){
        socket.broadcast.emit('reloadproductiontag');
    });

    socket.on('productionjorequestnotif', function(data){
        socket.emit('notifproductionjorequest', { data: data });        
    });

    //Finish Goods Sockets
    socket.on('fginvinformation', function(){
        socket.broadcast.emit('reloadfginvinformation');
    });

    //JO Monitoring Sockets
    socket.on('monitoringjo', function(){
        socket.broadcast.emit('reloadjomonitor');
    });

    //Delivery Sockets
    socket.on('deliverysalesinformation', function(){
        socket.broadcast.emit('reloaddeliverysalesinformation');
    });
    
    socket.on('fgpreparationslip', function(){
        socket.broadcast.emit('reloadfgpreparationslip');
    });


});